module Add6 (a, b, s, o);	// s = a + b
	input [5:0] a, b;
	output [5:0] s;
	output o;	// overflow flag
	
	wire [4:0] c;
	FullAdder fa0(a[0], b[0], 1'b0, s[0], c[0]),
		fa1(a[1], b[1], c[0], s[1], c[1]),
		fa2(a[2], b[2], c[1], s[2], c[2]),
		fa3(a[3], b[3], c[2], s[3], c[3]),
		fa4(a[4], b[4], c[3], s[4], c[4]),
		fa5(a[5], b[5], c[4], s[5], o);
endmodule

module Negative6 (en, in, out);	// 2's complement
	input en;	// enable
	input [5:0] in;
	output [5:0] out;
	wire [5:0] s;	// result
	wire o;	// overflow
	Add6 add(~in, 6'b000001, s, o);
	
	assign out = en ? s : in;
endmodule

module FullAdder (a, b, c_in, s, c_out);
	input a, b, c_in;	// a, b, carry input
	output s, c_out;	// sum, carry output
	
	assign s = a ^ b ^ c_in;
	assign c_out = a & b | c_in & (a ^ b);
endmodule

module LED7 (in, out);	// 7-segment display map
	input [3:0] in;
	output reg [6:0] out;

	always @(in) begin
		case (in)
			4'b0000: out = 7'b1000000;
			4'b0001: out = 7'b1111001;
			4'b0010: out = 7'b0100100;
			4'b0011: out = 7'b0110000;
			4'b0100: out = 7'b0011001;
			4'b0101: out = 7'b0010010;
			4'b0110: out = 7'b0000010;
			4'b0111: out = 7'b1011000;
			4'b1000: out = 7'b0000000;
			4'b1001: out = 7'b0010000;
			4'b1010: out = 7'b0001000;
			4'b1011: out = 7'b0000011;
			4'b1100: out = 7'b1000110;
			4'b1101: out = 7'b0100001;
			4'b1110: out = 7'b0000110;
			4'b1111: out = 7'b0001110;
			default: out = 7'b1111111;
		endcase
	end
endmodule

module Blink8 (en, CLK, LED);	// make LED blink at about 5Hz
	input en, CLK;	// enable, clock
	output [7:0] LED;
	reg [22:0] cnt;
	
	assign LED = (en & cnt[22]) ? 7'b1111111 : 7'b0000000;
	
	always @(posedge CLK) begin
		cnt <= cnt + 1;
	end
endmodule
