module DSLLAB1 (SW, HEX0, HEX1, HEX2, HEX4, HEX5, HEX6, HEX7, LEDR, LEDG, CLOCK_50);
	input [17:0] SW;	// switches
	input CLOCK_50;	// 50MHz clock
	output [6:0] HEX0, HEX1, HEX2, HEX4, HEX5, HEX6, HEX7;	// 7-segment display
	output [17:0] LEDR;	// red LEDs
	output [7:0] LEDG;	// green LEDs
	
	wire [5:0] in1, in2;	// inputs
	wire op;	// operator
	wire [5:0] nb, s;	// negative b (subtraction mode), sum
	wire [6:0] ns;	// negative sum
	wire o;	// overflow flag
	Negative6 neg1(op, in2, nb);
	Negative6 neg2(op & s[5], s, ns[5:0]);
	Add6 add(in1, nb, s, o);
	LED7 sa0(in1[3:0], HEX6), sa1({2'b00, in1[5:4]}, HEX7);
	LED7 sb0(in2[3:0], HEX4), sb1({2'b00, in2[5:4]}, HEX5);
	LED7 ss0(ns[3:0], HEX0), ss1({1'b0, ns[6:4]}, HEX1);
	Blink8 bl((op & ((~in1[5] & in2[5] & s[5]) | (in1[5] & ~in2[5] & ~s[5]))), CLOCK_50, LEDG);
	
	assign in1 = SW[5:0], in2 = SW[11:6];
	assign op = SW[17];
	assign HEX2 = (op & s[5]) ? 7'b0111111 : 7'b1111111;
	assign ns[6] = op ? 1'b0 : o;	// 7-bit result for addition
	assign LEDR[6:0] = {op ? 1'b0 : o, s};
endmodule
